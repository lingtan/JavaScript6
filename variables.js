let a;
let name = "Simon";

// myLetVariable is *not* visible out here
//console.log(myLetVariable);
for (let myLetVariable = 0; myLetVariable < 5; myLetVariable++) {
    // myLetVariable is only visible in here
}
// myLetVariable is *not* visible out here

const Pi = 3.14; // variable Pi is set
//Pi = 1; // will throw an error because you cannot change a constant variable.

console.log(name);

var v;
var v_name = 'new name';


// myVarVariable *is* visible out here
console.log(myVarVariable);
for (var myVarVariable = 0; myVarVariable < 5; myVarVariable++) {
    // myVarVariable is visible to the whole function
}
// myVarVariable *is* visible out here