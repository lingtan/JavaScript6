function makeAdder(a) {
    return function (b) {
        return a + b;
    }
}

var x = makeAdder(5);
var y = makeAdder(20);

x(6);
y(7);


/**
count is global
*/
var count = 0;
function counter(){
	count = count + 1;
	return count;
}

// with closure
function makeCounter(){
	var count = 0;
	function counter(){
		count = count + 1;
		return count;
	}
	return counter;
}
var doCount = makeCounter();
console.log(doCount());
console.log(doCount());
console.log(doCount());


// another example
function makeTimer(doneMessage, n){
	setTimeout(function(){
		alert(doneMessage);
	}, n);
}
makeTimer("Cookies are done!", 1000);


// another example
function makeTimer(doneMessage, n){
	setTimeout(function(){
		alert(doneMessage);
	}, n);
	doneMessage = "OUCH!";
}
makeTimer("Cookies are done!", 1000);
