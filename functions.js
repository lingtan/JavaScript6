function add(x, y) {
    var total = x + y;
    return total;
}

add(); // NaN
add(2, 3, 4); // 5

function add1() {
    var sum = 0;
    for (var i = 0, j = arguments.length; i < j; i++) {
        sum += arguments[i];
    }
    return sum;
}
add1(2, 3, 4, 5); // 14

function avg() {
    var sum = 0;
    for (var i = 0, j = arguments.length; i < j; i++) {
        sum += arguments[i];
    }
    return sum / arguments.length;
}

avg(2, 3, 4, 5); // 3.5

function avg1(...args) {
    var sum = 0;
    for (let value of args) {
        sum += value;
    }
    return sum / args.length;
}

avg1(2, 3, 4, 5); // 3.5