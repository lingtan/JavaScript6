var a = new Array();
a[0] = 'dog';
a[1] = 'cat';
a[2] = 'hen';
a.length; // 3

var b = ['dog', 'cat', 'hen'];
b.length; // 3

var c = ['dog', 'cat', 'hen'];
c[100] = 'fox';
c.length; // 101
console.log(typeof c[90]); // undefined

for (var i = 0; i < a.length; i++) {
    // Do something with a[i]
}

for (const currentValue of a) {
    // Do something with currentValue
}

['dog', 'cat', 'hen'].forEach(function(currentValue, index, array) {
    // Do something with currentValue or array[index]
});

a.push('');