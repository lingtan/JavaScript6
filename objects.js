var obj1 = new Object();
var obj2 = {};

var obj = {
    name: 'Carrot',
    for: 'Max', // 'for' is a reserved word, use '_for' instead.
    details: {
        color: 'orange',
        size: 12
    }
};

console.log(obj.details.color); // orange
console.log(obj['details']['size']); // 12

console.log(obj.for);
console.log(obj['for']);

function Person(name, age) {
    this.name = name;
    this.age = age;
}

// Define an object
var you = new Person('You', 24);
console.log(you.name);
console.log(you.age);
// We are creating a new person named "You" aged 24.

// bracket notation
obj['name'] = 'Simon';
var name = obj['name'];
// can use a variable to define a key
//var user = window.prompt('what is your key?')
//obj[user] = window.prompt('what is its value?')

var obj0 = {};
obj0.for = 'Simon0'; // Syntax error, because 'for' is a reserved word (works fine)
//obj0['for'] = 'Simon'; // works fine

console.log(obj0.for);