0.1 + 0.2 == 0.3;//false
0.1 + 0.2 == 0.30000000000000004;//true

Math.sin(3.5);
parseInt('123', 10);

console.log(parseInt('123', 10));
console.log(parseInt('123.45', 10));

console.log(parseFloat('123'));
console.log(parseFloat('123.45'));

console.log(parseInt('hello', 10));
console.log(parseInt('123hello', 10));

console.log(+ '123');
console.log(+ '123.45');
console.log(+ 'hello');
console.log(+ '123hello');

console.log(Number('123'));
console.log(Number('123.45'));
console.log(Number('hello'));
console.log(Number('123hello'));
