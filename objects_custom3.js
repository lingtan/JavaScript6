function Person(first, last) {
    this.first = first;
    this.last = last;
    this.fullName = function() {
        return this.first + ' ' + this.last;
    };
    this.fullNameReversed = function() {
        return this.last + ', ' + this.first;
    };
}
var s = new Person('Simon', 'Willison');

//Every time we create a person object we are creating two brand new function objects within it — wouldn't it be better if this code was shared?