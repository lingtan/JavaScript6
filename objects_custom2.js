function makePerson(first, last) {
    return {
        first: first,
        last: last,
        fullName: function () {
            return this.first + ' ' + this.last;
        },
        fullNameReversed: function () {
            return this.last + ', ' + this.first;
        }
    };
}

s = makePerson('Simon', 'Willison');
s.fullName(); // "Simon Willison"
s.fullNameReversed(); // "Willison, Simon"

//Used inside a function, "this" refers to the current object.
// What that actually means is specified by the way in which you called that function.
// If you called it using dot notation or bracket notation on an object, that object becomes this.
// If dot notation wasn't used for the call, this refers to the global object.