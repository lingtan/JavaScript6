# JavaScript: Understand JavaScript Prototype


[JavaScript Prototype in Plain Language](http://javascriptissexy.com/javascript-prototype-in-plain-detailed-language/)

```JavaScript
// The userAccount object inherits from Object and as such its prototype attribute is Object.prototype.
var userAccount = new Object();
// This demonstrates the use of an object literal to create the userAccount object; the userAccount
// object inherits from Object; therefore, its prototype attribute is Object.prototype just as the
// userAccount object does above.
var userAccount = {name: “Mike”};

function Account() {
}
// userAccount initialized with the Account () constructor and as such its prototype attribute (or 
// prototype object) is Account.prototype.
var userAccount = new Account();
```

```JavaScript
function Plant () {
  this.country = "Mexico";
  this.isOrganic = true;
}

// Add the showNameAndColor method to the Plant prototype property
Plant.prototype.showNameAndColor =  function () {
  console.log("I am a " + this.name + " and my color is " + this.color);
}

// Add the amIOrganic method to the Plant prototype property
Plant.prototype.amIOrganic = function () {
  if (this.isOrganic)
    console.log("I am organic, Baby!");
}

function Fruit (fruitName, fruitColor) {
  this.name = fruitName;
  this.color = fruitColor;
}

// Set the Fruit's prototype to Plant's constructor, thus inheriting all of Plant.prototype methods 
// and properties.
Fruit.prototype = new Plant();

// Creates a new object, aBanana, with the Fruit constructor
var aBanana = new Fruit ("Banana", "Yellow");

// Here, aBanana uses the name property from the aBanana object prototype, which is Fruit.prototype:
console.log(aBanana.name); // Banana

// Uses the showNameAndColor method from the Fruit object prototype, which is Plant.prototype. 
// The aBanana object inherits all the properties and methods from both the Plant and Fruit functions.
console.log(aBanana.showNameAndColor()); // I am a Banana and my color is yellow.
```
