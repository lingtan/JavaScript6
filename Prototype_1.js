function Dog(name, breed, weight){
	this.name = name;
	this.breed = breed;
	this.weight = weight;
}

Dog.prototype.sitting = false;

Dog.prototype.sit = function(){
	if(this.sitting){
		console.log(this.name + " is already sitting.");
	}else{
		this.sitting = true;
		console.log(this.name + " is now sitting.");
	}
};

var fido = new Dog("Fido", "Mixed", 38);
fido.sit();// Fido is now sitting.
fido.sit();// Fido is already sitting.
