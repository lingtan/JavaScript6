function Dog(name, breed, weight){
	this.name = name;
	this.breed = breed;
	this.weight = weight;
}

var fido = new Dog("Fido", "Mixed", 38);

// 1. "new" create a new, empty object;
// 2. "new" set "this" to point to the new object;
// 3. call the function "Dog", passing "Fido, "Mixed", 38 as arguments.
// 4. the body of the function is invoked.
// 5. the "new" operator return "this", which is a reference to the newly created object.
// 6. we assign that reference to the variable "fido"


// What if we forget "new"
//	no new object would be created.
//	"this" will refer to the global object of your application. (usually is the window ojbect)
//	no object is returned from the constructor
//	no object is assigned to the "fido"
//	"fido" is undefined
